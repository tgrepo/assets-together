import { Component } from '@angular/core';

interface TextModel {
  title1: string;
  title2: string;
  text: string;
  button_text: string;
  email_placeholder_text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  RU = {
    title1: 'Совместные инвестиции',
    title2: 'Доходность до 10% в год',
    text: 'Согласно данному договору все его участники образуют совместный долевой капитал путем вложения каждым из них своей доли. Согласно прописанным обязанностям в договоре каждая из сторон занимается той деятельностью, которая ей поручена, если иное не предусмотрено. Прибыль и выгоды от совместных инвестиций распределяется между всеми его участниками пропорционально размеру первоначально инвестированной доли.Assets Together открывает для широкого круга потенциальных инвесторов инструменты, которыми до сих пор могли пользоваться только индивидуально богатые вкладчики. Высокая стоимость сделок, затраты на поиск и анализ объектов, оформление сделок и управление активами делают самостоятельное инвесторование в коммерческую недвижимость недоступным для тех, кто ещё не стал миллионером. Мы составляем группы для совместного инвестирования в подобранные и изученные нами объекты коммерческой недвижимости на Украине. Сумма инвестиции каждого из со-инвесторов составляет 5-10 тысяч долларов, в зависимости от объекта. Годовая норма прибыли - до 10%.',
    button_text: 'контактные данные',
    email_placeholder_text: 'имэйл'
  } as TextModel;

  HE = {
    title1: 'השקעות קבוצתיות',
    title2: 'רווח עד 10% שנתית',
    text: 'קבוצות השקעה דמיינו לעצמכם את הסיטואציה הבאה: אתם יושבים במשרד שלכם, כרגיל בודקים ועונים למיילים, עוברים על לוח הזמנים שלכם לאותו היום, למעשה הכל מתנהל כרגיל, רק שבמקביל למשרה הקבועה שלכם, אתם נהנים מתשואה נוספת הנוצרת מהשקעה אותה בה השקעתם באמצעות הון עצמי אותו חסכתם במהלך השנים. אז כן, מסתבר שאין צורך לוותר על המשרה הרגילה שלכם. אין צורך לוותר על סולם הקריירה בו השקעתם את כל כולכם, למדתם והגעתם למקום של כבוד. במקום לחפש דרך עצמאית שאיננה פשוטה להתמודדות, דרך שבאחוז גבוה מהמקרים מובילה לכישל..',
    button_text: 'קבל מידע נוסף',
    email_placeholder_text: 'דואל אלקטרוני'
  } as TextModel;

  curentTexts: TextModel

  constructor() {
    this.setRU();
  }

  isRTL() {
    return this.isHE();
  }

  isHE() {
    return this.curentTexts === this.HE;
  }

  isRU() {
    return this.curentTexts === this.RU;
  }

  setRU() {
    this.curentTexts = this.RU;
  }

  setHE() {
    this.curentTexts = this.HE;
  }

  getLanguageButtonText() {
    if (this.isHE()) {
      return 'RU';
    }
    return 'HE';
  }

  switchLanguage() {
    if (this.isHE()) {
      this.setRU();
    } else {
      this.setHE();
    }
  }

  getMaintTitle1() {
    return this.curentTexts.title1;
  }

  getMaintTitle2() {
    return this.curentTexts.title2;
  }

  getMainText() {
    return this.curentTexts.text;
  }

  getButtonText() {
    return this.curentTexts.button_text;
  }

  getEmailPlaceholderText() {
    return this.curentTexts.email_placeholder_text;
  }

}
