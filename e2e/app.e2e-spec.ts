import { AssetsTogetherPage } from './app.po';

describe('assets-together App', function() {
  let page: AssetsTogetherPage;

  beforeEach(() => {
    page = new AssetsTogetherPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
